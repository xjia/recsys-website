<?php
include_once(__DIR__ . '/inc/init.php');

if (is_user_logged_in()) {
	logout_current_user();
	redirect_to('login.php');
} else {
	redirect_to('login.php');
}
<?php
include_once(__DIR__ . '/inc/init.php');

if (!is_user_logged_in()) {
	redirect_to('login.php');
}

$id = param('id');
if (!is_valid_item_id($db, $id)) {
	redirect_to('index.php');
}
if (is_disabled_item($db, $id)) {
  redirect_to('index.php');
}
$item = get_item($db, $id);
$images = get_item_images($db, $id);
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>评分测试 | RecSys</title>
	<link rel="stylesheet" type="text/css" href="css/base.css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="css/item.css" media="screen"/>
</head>
<body id="item">
	<div id="item-box">
		<div id="content">
			<div id="logout"><a href="logout.php">切换用户</a></div>
			<div id="viewer">
				<div id="gallery">
					<div class="booth"><img class="pic" src=""/></div>
					<ul class="thumbs">
					<?php foreach ($images as $image): ?>
						<li title="鼠标移动到上面的大图可以放大"><img class="thumb" src="http://download.acm-project.org/recsys/<?php echo $item['product_id']; ?>/top/<?php echo $image['file_name']; ?>"/></li>
					<?php endforeach; ?>
					</ul>
				</div>
				<div id="magnified"><img src=""/></div>
				<div id="help">
					<p class="first-para">
						<span class="username"><?php echo current_user(); ?></span>，您好！
					</p>
					<p>
						感谢您能花时间为我们的项目做测试哦<span class="smiley">O(∩_∩)O~</span>
					</p>
					<p>
						为使网站能使用的方便有效，这里有一些小tips：
						<ol>
							<li>请根据您对这件衣服的喜好打分，即您有多大意愿拥有这件衣服</li>
							<li>打分时请忽略价格和颜色因素，以衣服款式为主</li>
							<li>鼠标移至左下方的小图上可以切换至衣服的不同角度</li>
							<li>鼠标移至左边的大图上可以看到放大的细节图</li>
							<li>以点亮几颗星星表示您给这件衣服的分数，5分表示非常喜欢，1分表示很差</li>
						</ol>
					</p>
					<p class="rating-para">
						<?php $current_rating = get_current_rating($db, current_user(), $item['id']); ?>
						<ul class="star-rating">
							<li class="current-rating"><?php echo $current_rating; ?></li>
							<li><a href="<?php echo rate_path($item, 1); ?>" title="很差" class="one-star">1</a></li>
							<li><a href="<?php echo rate_path($item, 2); ?>" title="不好" class="two-stars">2</a></li>
							<li><a href="<?php echo rate_path($item, 3); ?>" title="一般" class="three-stars">3</a></li>
							<li><a href="<?php echo rate_path($item, 4); ?>" title="喜欢" class="four-stars">4</a></li>
							<li><a href="<?php echo rate_path($item, 5); ?>" title="非常喜欢" class="five-stars">5</a></li>
						</ul>
					</p>
					<p class="count-para">
						您已经评价过<span class="rating-count"><?php echo get_number_of_rated_items($db, current_user()); ?></span>件衣服了
					</p>
				</div>
			</div>
			<div id="attributes" class="attributes">
				<ul class="attributes-list">
					<li><?php echo preg_replace('/[\r\n]+/', '</li><li>', trim($item['description'])); ?></li>
				</ul>
			</div>
		</div>
	</div>
	<img id="lens" src="img/zoom.png"/>
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/item.js"></script>
	<script type="text/javascript">
	$(function(){
		$(window).load(function(){
			$('.current-rating').css('width', '<?php echo $current_rating * 30; ?>px');
			$('.rating-para').show();
			$('.star-rating').show();
			$('.count-para').show();
		});
	});
	</script>
</body>
</html>
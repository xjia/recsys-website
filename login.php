<?php
include_once(__DIR__ . '/inc/init.php');

if (is_user_logged_in()) {
	redirect_to('index.php');
}

$errmsg = '';
if (is_post_request()) {
	$username = param('username');
	$password = param('password');
	if (empty($username)) {
		$errmsg = '请输入用户名';
	} else if (empty($password)) {
		$errmsg = '请确认用户名';
	} else if (!login($username, $password)) {
		$errmsg = '两次输入的用户名不一致，请再试一次 :-)';
	} else {
		redirect_to('index.php');
	}
}
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>登录 | RecSys</title>
	<link rel="stylesheet" type="text/css" href="css/base.css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="css/login.css" media="screen"/>
</head>
<body id="login">
	<div id="login-box">
		<div id="content">
			<div id="login-form-wrap">
				<h1 id="logotype">RecSys</h1>
				<div id="flash-block"><?php echo $errmsg; ?></div>
				<div>
					<form action="login.php" id="login-form" method="POST" autocomplete="off">
						<fieldset>
							<legend>登录</legend>
							<label for="username">
								<span class="hint fn-right">请输入我们分发给您的用户名（一个单词）</span>
								用户名
							</label>
							<input type="text" name="username" value="<?php echo param('username'); ?>" tabindex="1" id="username"/>
							<label for="password">
								<span class="hint fn-right">为避免您输入错误，请再输入一次上述单词</span>
								确认
							</label>
							<input type="password" name="password" tabindex="2" id="password"/>
							<input type="submit" value="登录" class="button fn-left" tabindex="3" id="submit"/>
						</fieldset>
					</form>
					<script type="text/javascript">
						document.getElementById('username').focus();
					</script>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<?php
function in_range($value, $min, $max)
{
	return $min <= $value and $value <= $max;
}

function fatal_error($msg)
{
	echo $msg;
	exit;
}

function redirect_to($url)
{
	header('Location: ' . $url);
	exit;
}

function now()
{
	return date('Y-m-d H:i:s');
}

function param($key)
{
	if (array_key_exists($key, $_POST)) {
		return $_POST[$key];
	}
	if (array_key_exists($key, $_GET)) {
		return $_GET[$key];
	}
	return '';
}

function is_post_request()
{
	return !empty($_POST);
}

function is_user_logged_in()
{
	return array_key_exists('user', $_SESSION) and !empty($_SESSION['user']);
}

function current_user()
{
	return $_SESSION['user'];
}

function is_root($username)
{
	return '56c87d10856d6b049d948a2466d88a52' == md5($username);
}

function is_root_password($password)
{
	return '3d1ec02963442fc7497cf88bd242d149' == md5($password);
}

function set_current_user($username)
{
	$_SESSION['user'] = $username;
}

function login($username, $password)
{
	$username = strtolower($username);
	$password = strtolower($password);
	if (is_root($username)) {
		if (is_root_password($password)) {
			set_current_user($username);
			return true;
		} else {
			return false;
		}
	} else {
		if ($username == $password) {
			set_current_user($username);
			return true;
		} else {
			return false;
		}
	}
}

function logout_current_user()
{
	$_SESSION['user'] = '';
}

function is_valid_integer($n)
{
	return preg_match('/\d+/', $n);
}

function item_exists($db, $id)
{
	$result = $db->translatedQuery('SELECT COUNT(*) FROM items WHERE id=%i', $id);
	return $result->fetchScalar() > 0;
}

function product_exists($db, $product_id)
{
	$result = $db->translatedQuery('SELECT COUNT(*) FROM items WHERE product_id=%s', $product_id);
	return $result->fetchScalar() > 0;
}

function get_item($db, $id)
{
	return $db->translatedQuery('SELECT * FROM items WHERE id=%i LIMIT 1', $id)->fetchRow();
}

function get_item_by_product_id($db, $product_id)
{
	return $db->translatedQuery('SELECT * FROM items WHERE product_id=%s LIMIT 1', $product_id)->fetchRow();
}

function get_item_images($db, $item_id)
{
	return $db->translatedQuery('SELECT * FROM images WHERE item_id=%i', $item_id)->fetchAllRows();
}

function is_valid_item_id($db, $id)
{
	return is_valid_integer($id) and item_exists($db, $id);
}

function is_valid_score_level($level)
{
	return is_valid_integer($level) and in_range($level, 1, 5);
}

function is_disabled_item($db, $item_id)
{
  return get_current_rating($db, 'filter', $item_id) == 1;
}

function get_unrated_item_randomly($db, $user)
{
	$result = $db->translatedQuery(
		'SELECT items.id FROM items LEFT OUTER JOIN scores ' .
		'ON items.id=scores.item_id AND scores.user_name=%s ' .
		'WHERE scores.item_id IS NULL LIMIT 1000', $user);
	$num_of_rows = $result->countReturnedRows();
	if ($num_of_rows > 0) {
		$result->seek(mt_rand(0, $num_of_rows - 1));
		$row = $result->fetchRow();
		if (is_disabled_item($db, $row['id'])) {
		  return get_unrated_item_randomly($db, $user);
		}
		return $row['id'];
	}
	return -1;
}

function get_number_of_rated_items($db, $user)
{
	return $db->translatedQuery(
		'SELECT COUNT(DISTINCT item_id) FROM scores WHERE user_name=%s',
		$user)->fetchScalar();
}

function rate_path($item, $level)
{
	return 'score.php?id=' . $item['id'] . '&level=' . $level;
}

function create_score($db, $user, $item_id, $level)
{
	return $db->translatedQuery(
		'INSERT INTO scores(user_name, item_id, level, created_at) VALUES(%s, %i, %i, %s)',
		$user, $item_id, $level, now())->getAutoIncrementedValue();
}

function import_score($db, $user, $item_id, $level, $create_time)
{
	return $db->translatedQuery(
		'INSERT INTO scores(user_name, item_id, level, created_at) VALUES(%s, %i, %i, %s)',
		$user, $item_id, $level, $create_time)->getAutoIncrementedValue();
}

function is_rated($db, $item_id, $user)
{
	return get_current_rating($db, $user, $item_id) > 0;
}

function get_current_rating($db, $user, $item_id)
{
	$result = $db->translatedQuery(
		'SELECT level FROM scores ' .
		'WHERE user_name=%s AND item_id=%i ' .
		'ORDER BY updated_at DESC LIMIT 1', $user, $item_id);
	if ($result->countReturnedRows() > 0) {
		return $result->fetchScalar();
	}
	return 0;
}

function get_rating_stat($db)
{
	return $db->translatedQuery(
		'SELECT user_name, COUNT(DISTINCT item_id) AS count, AVG(level) AS average ' .
		'FROM scores GROUP BY user_name ORDER BY count DESC');
}

function str_contains($haystack, $needle)
{
	return strpos($haystack, $needle) !== FALSE;
}

function is_valid_description($desc)
{
	return str_contains($desc, '衣') and !str_contains($desc, '裤') and !str_contains($desc, '男女款:男');
}

function remove_all_items($db)
{
	$db->translatedExecute('DELETE FROM items');
}

function remove_all_images($db)
{
	$db->translatedExecute('DELETE FROM images');
}

function remove_all_scores($db)
{
	$db->translatedExecute('DELETE FROM scores');
}

function create_item($db, $product_id, $description)
{
	return $db->translatedQuery(
		'INSERT INTO items(product_id, description, created_at) VALUES(%i, %s, %s)',
		$product_id, $description, now())->getAutoIncrementedValue();
}

function create_image($db, $item_id, $file_name)
{
	return $db->translatedQuery(
		'INSERT INTO images(item_id, file_name, created_at) VALUES(%i, %s, %s)',
		$item_id, $file_name, now())->getAutoIncrementedValue();
}
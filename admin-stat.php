<?php
include_once(__DIR__ . '/inc/init.php');

if (!is_user_logged_in() or !is_root(current_user())) {
	redirect_to('index.php');
}
?>
<table>
<thead>
	<tr>
		<th>#</th>
		<th>User name</th>
		<th>Rated number</th>
		<th>Average score</th>
	</tr>
</thead>
<tbody>
<?php foreach (get_rating_stat($db) as $i => $row): ?>
	<tr>
		<td><?php echo $i + 1; ?></td>
		<td><?php echo $row['user_name']; ?></td>
		<td><?php echo $row['count']; ?></td>
		<td><?php echo $row['average']; ?></td>
	</tr>
<?php endforeach; ?>
</tbody>
</table>
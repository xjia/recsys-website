<?php
include_once(__DIR__ . '/inc/init.php');

if (is_user_logged_in()) {
	if (is_valid_item_id($db, param('id')) and is_valid_score_level(param('level'))) {
		create_score($db, current_user(), param('id'), param('level'));
		redirect_to('index.php');
	} else {
		redirect_to('index.php');
	}
} else {
	redirect_to('login.php');
}
<?php
include_once(__DIR__ . '/inc/init.php');

if (!is_user_logged_in() or !is_root(current_user())) {
	redirect_to('index.php');
}

set_time_limit(0);

$sqlite_file = param('sqlite_file');
if (empty($sqlite_file)) {
	fatal_error('Need parameter sqlite_file');
}
$sqlite_db = new fDatabase('sqlite', $sqlite_file);

$new_score_ids = array();
$all_scores = $sqlite_db->translatedQuery('SELECT * FROM scores');
foreach ($all_scores as $r) {
	$old_item_id = $r['item_id'];
	$old_item = get_item($sqlite_db, $old_item_id);
	$product_id = $old_item['product_id'];
	if (!product_exists($db, $product_id)) continue;
	
	$item = get_item_by_product_id($db, $product_id);
	$item_id = $item['id'];
	if (is_valid_item_id($db, $item_id) 
			and is_valid_score_level($r['level']) 
			and !is_rated($db, $item_id, $r['user_name'])) {
		$new_score_ids[] = import_score($db, $r['user_name'], $item_id, $r['level'], $r['created_at']);
	}
}
echo 'Imported ' . count($new_score_ids) . ' scores.';

$joint_ids = implode(',', $new_score_ids);
$log_filename = date('Ymd-His') . '.log';
file_put_contents($log_filename, $joint_ids);
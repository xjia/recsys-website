<?php
include_once(__DIR__ . '/inc/init.php');

if (is_user_logged_in()) {
	$item_id = get_unrated_item_randomly($db, current_user());
	if ($item_id >= 0) {
		redirect_to('item.php?id=' . $item_id);
	} else {
		redirect_to('404.html');
	}
} else {
	redirect_to('login.php');
}
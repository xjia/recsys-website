$(function() {
	var booth = $('.booth'),
		offset = booth.offset(),
		lens = $('#lens');
	lens.css('left', offset.left + booth.width() - lens.width());
	lens.css('top', offset.top + booth.height() - lens.height());
	lens.show();
	lens.mouseover(function(){
		$(this).hide();
	});
	
	$('.booth .pic').mouseover(function(){
		var mag = $('#magnified'),
			img = $('#magnified img'),
			help_pos = $('#help').position();
		img.attr('src', $(this).attr('src'));
		mag.css('left', help_pos.left);
		mag.css('top', help_pos.top);
		mag.show();
		lens.hide();
	}).mousemove(function(e){
		var bw = $(this).width(),
			bh = $(this).height(),
			mag = $('#magnified'),
			mw = mag.width(),
			mh = mag.height(),
			img = $('#magnified img'),
			iw = img.width(),
			ih = img.height(),
			dw = Math.max(iw - mw, 0),
			dh = Math.max(ih - mh, 0),
			offset = $(this).offset(),
			x = e.pageX - offset.left,
			y = e.pageY - offset.top;
		img.css('left', -dw * x / bw);
		img.css('top', -dh * y / bh);
	}).mouseout(function(){
		$('#magnified').hide();
		lens.show();
	});
	
	$('.thumb').mouseover(function(){
		$('.thumbs .selected').removeClass('selected');
		$(this).parent().addClass('selected');
		$('.booth .pic').attr('src', $(this).attr('src'));
	});
	$('.thumb').first().mouseover();
});
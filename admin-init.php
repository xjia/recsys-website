<?php
include_once(__DIR__ . '/inc/init.php');

if (!is_user_logged_in() or !is_root(current_user())) {
	redirect_to('index.php');
}

$init_dir = param('init_dir');
if (empty($init_dir)) {
	fatal_error('Need parameter init_dir');
}

if (!chdir($init_dir)) {
	fatal_error('Failed to chdir ' . $init_dir);
}

set_time_limit(0);

remove_all_items($db);
remove_all_images($db);
remove_all_scores($db);

foreach (scandir('.') as $product_id) {
	if (preg_match('/\d+/', $product_id)) {
		if (chdir($product_id)) {
			$image_file_names = array();
			foreach (scandir('top') as $image_file_name) {
				if (preg_match('/\.jpg$/', $image_file_name)) {
					$image_file_names[] = $image_file_name;
				}
			}
			$description = file_get_contents($product_id . '.txt');
			if (!empty($image_file_names) and is_valid_description($description)) {
				$item_id = create_item($db, $product_id, $description);
				echo 'Item created: ' . $item_id . '<BR/>';
				foreach ($image_file_names as $image_file_name) {
					create_image($db, $item_id, $image_file_name);
				}
				echo 'Images created for item ' . $item_id . '<BR/>';
			}
			chdir('..');
		} else {
			echo 'Failed to chdir of product ' . $product_id . '<BR/>';
		}
	}
}